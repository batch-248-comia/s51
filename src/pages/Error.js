// import {Row, Col, Button} from 'react-bootstrap';
// import { Link } from 'react-router-dom';


// export default function NotFound(){

// 	return(
// 		<>
		
// 		<Row className="mt-3">
// 			<Col>
// 				<h1> Page Not Found</h1>
// 				<p>The page you are looking for cannot be found.</p>
// 				<Button variant="primary" as={Link} to="/">Back to Home</Button>
// 			</Col>
// 		</Row>
// 		</>
// 		)
// }

import Banner from '../components/Banner';


export default function Error(){

	const data = {

		title: "404 - Page Not Found",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back to Home"

	}

	return (

		<Banner data={data}/>

	)

}